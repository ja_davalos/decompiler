package com.assoc.jad.decompiler.classformat;

public class ConstantPoolDouble {

	    private byte tag;
	    private byte[] lowBytes = new byte[4];
	    private byte[] highBytes = new byte[4];
	    
		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
		public byte[] getLowBytes() {
			return lowBytes;
		}
		public void setLowBytes(byte[] lowBytes) {
			this.lowBytes = lowBytes;
		}
		public byte[] getHighBytes() {
			return highBytes;
		}
		public void setHighBytes(byte[] highBytes) {
			this.highBytes = highBytes;
		}
}

