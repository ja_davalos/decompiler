package com.assoc.jad.decompiler.classformat;

public class ConstantPoolInteger {

	    private byte tag;
	    private byte[] bytes = new byte[4];
	    
		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
		public byte[] getBytes() {
			return bytes;
		}
		public void setBytes(byte[] bytes) {
			this.bytes = bytes;
		}
}

