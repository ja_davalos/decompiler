package com.assoc.jad.decompiler.bytecode;

import java.util.HashMap;

public class InstructionClazzes {
	
	public static HashMap<Byte, IInstructions> instructions = instructionsInit();
	
	private static HashMap<Byte,IInstructions> instructionsInit() {
		
		HashMap<Byte, IInstructions> allInstructions = new HashMap<>(300);
		
		allInstructions.put((byte)0x02, new IntConst_X(-1));
		allInstructions.put((byte)0x03, new IntConst_X(0));
		allInstructions.put((byte)0x04, new IntConst_X(1));
		allInstructions.put((byte)0x05, new IntConst_X(2));
		allInstructions.put((byte)0x06, new IntConst_X(3));
		allInstructions.put((byte)0x07, new IntConst_X(4));
		allInstructions.put((byte)0x08, new IntConst_X(5));
		allInstructions.put((byte)0x12, new Ldc());
		allInstructions.put((byte)0x18, new Dload());
		allInstructions.put((byte)0x1a, new IntLoad_X(0));
		allInstructions.put((byte)0x1b, new IntLoad_X(1));
		allInstructions.put((byte)0x1c, new IntLoad_X(2));
		allInstructions.put((byte)0x1d, new IntLoad_X(3));
		allInstructions.put((byte)0x2a, new Aload_X(0));
		allInstructions.put((byte)0x2b, new Aload_X(1));
		allInstructions.put((byte)0x2c, new Aload_X(2));
		allInstructions.put((byte)0x2d, new Aload_X(3));
		allInstructions.put((byte)0x26, new DLoad_X(0));
		allInstructions.put((byte)0x27, new DLoad_X(1));
		allInstructions.put((byte)0x28, new DLoad_X(2));
		allInstructions.put((byte)0x29, new DLoad_X(3));
		allInstructions.put((byte)0x36, new IntStore());
		allInstructions.put((byte)0x3b, new IntStore_X(0));
		allInstructions.put((byte)0x3c, new IntStore_X(1));
		allInstructions.put((byte)0x3d, new IntStore_X(2));
		allInstructions.put((byte)0x3e, new IntStore_X(3));
		allInstructions.put((byte)0xa7, new GoTo());
		allInstructions.put((byte)0xb1, new Return());
		allInstructions.put((byte)0xb7, new InvokeSpecial());
		allInstructions.put((byte)0xb5, new PutField());
		
		allInstructions.put((byte)0x11, new SIPush());
		allInstructions.put((byte)0xbb, new NewObj());
		allInstructions.put((byte)0x59, new Dup());
		allInstructions.put((byte)0xb8, new InvokeStatic());
		allInstructions.put((byte)0xb6, new InvokeVirtual());
		allInstructions.put((byte)0xb0, new AReturn());
		allInstructions.put((byte)0x60, new IntAdd());
		allInstructions.put((byte)0x15, new IntLoad());
		allInstructions.put((byte)0x84, new IntInc());
		allInstructions.put((byte)0x10, new ByteIntPush());
		allInstructions.put((byte)0x9f, new If_IntCmp("eq"));
		allInstructions.put((byte)0xa0, new If_IntCmp("ne"));
		allInstructions.put((byte)0xa1, new If_IntCmp("lt"));
		allInstructions.put((byte)0xa2, new If_IntCmp("ge"));
		allInstructions.put((byte)0xa3, new If_IntCmp("gt"));
		allInstructions.put((byte)0xa4, new If_IntCmp("le")); 

		return allInstructions;
	}
}
