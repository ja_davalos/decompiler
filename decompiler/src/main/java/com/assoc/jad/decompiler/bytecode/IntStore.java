package com.assoc.jad.decompiler.bytecode;

import java.nio.ByteBuffer;
import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

/**
 * <b>Store int into local variable</b>
 * The index is an unsigned byte that must be an index into the local variable array of the current frame (§2.6). 
 * The value on the top of the operand stack must be of type int. 
 * It is popped from the operand stack, and the value of the local variable at index is set to value.
 * @author jorge
 *
 */
public class IntStore implements IInstructions {
	private int disp = 1;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		Utilities utils = new Utilities();
		int lvIndex =  utils.getIndexFromCode(method, ndx,disp);
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[lvIndex];
		result = utils.BldLocalVariableText(lv,operandStack);
		return null;
	}

	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		return result.toString();
	}


}
