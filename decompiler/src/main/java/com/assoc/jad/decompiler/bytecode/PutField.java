package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

/*
 * load a reference onto the stack from local variable 0
 */
public class PutField implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method,int ndx,JavaClass javaClass, Stack<String> operandStack) {
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		
		ConstantPool constantPool = method.getConstantPool();
		ConstantFieldref fieldRef = (ConstantFieldref) constantPool.getConstant(cpIndex);
		ConstantNameAndType nameAndType = (ConstantNameAndType) constantPool.getConstant(fieldRef.getNameAndTypeIndex());
		
		String prevValue = operandStack.pop();
		Field[] fields = javaClass.getFields();
		for (int i=0;i<fields.length;i++) {
			if (!fields[i].getName().equals(nameAndType.getName(constantPool))) continue;
			result = new StringBuilder(fields[i].toString()).append(" = ").append(prevValue).append(';').append(System.lineSeparator());
		}
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}

}
