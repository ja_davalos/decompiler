package com.assoc.jad.decompiler.classformat;

import java.nio.ByteBuffer;

public class AttributeInfo {
    private byte[] attributeNameIndex = new byte[2];
    private byte[] attributeLength = new byte[4];
    private byte[] Info = null;
	private int UAttributeLength;
	
	private int ctr = 0;
	
	public byte[] getAttributeNameIndex() {
		return attributeNameIndex;
	}
	public void setAttributeNameIndex(byte[] attributeNameIndex) {
		this.attributeNameIndex = attributeNameIndex;
	}
	public byte[] getAttributeLength() {
		return attributeLength;
	}
	public void setAttributeLength(byte[] attributeLength) {
		this.attributeLength = attributeLength;
		UAttributeLength = ByteBuffer.wrap(attributeLength).getInt();
		Info = new byte[UAttributeLength];
		ctr = 0;
	}
	public int getUAttributeLength() {
		return UAttributeLength;
	}
	public void setUAttributeLength(int uAttributeLength) {
		UAttributeLength = uAttributeLength;
	}
	public byte[] getInfo() {
		return Info;
	}
	public void addInfo(byte info) {
		this.Info[ctr++] = info;
	}
}
