package com.assoc.jad.decompiler.classformat;

public class ConstantPoolInterfaceMethodRef {

	    private byte tag;
	    private byte[] classIndex = new byte[2];
	    private byte[] nameTypeIndex = new byte[2];

		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
		public byte[] getClassIndex() {
			return classIndex;
		}
		public void setClassIndex(byte[] classIndex) {
			this.classIndex = classIndex;
		}
		public byte[] getNameTypeIndex() {
			return nameTypeIndex;
		}
		public void setNameTypeIndex(byte[] nameTypeIndex) {
			this.nameTypeIndex = nameTypeIndex;
		}
}

