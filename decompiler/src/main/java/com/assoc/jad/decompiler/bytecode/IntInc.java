package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;
/**
 * <b>iinc Increment local variable by constant</b><br/>
 * The index is an unsigned byte that must be an index into the local variable array of the current frame (§2.6). 
 * The const is an immediate signed byte. The local variable at index must contain an int. The value const is first sign-extended to an int, 
 * and then the local variable at index is incremented by that amount.
 * 
 * @author jorge
 *
 */
public class IntInc implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method,int ndx,JavaClass javaClass,Stack<String> operandStack) {
		result = new StringBuilder();
		Utilities utils = new Utilities();
		int lvndx = utils.getIndexFromCode(method, ndx, 1);
		int intConst = method.getCode().getCode()[ndx+2]; //extends sign
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[lvndx];
		if (intConst == 1) result.append(lv.getName()).append("++").append(';');
		else result.append(lv.getName()).append(" = ").append(lv.getName()).append('+').append(intConst).append(';');
		result.append(System.lineSeparator());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {	
		return result.toString();
	}
}
