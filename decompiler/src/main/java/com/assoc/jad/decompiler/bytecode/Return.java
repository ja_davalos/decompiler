package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public class Return implements IInstructions {

	@Override
	public String execute(Method method, int i, JavaClass javaClass, Stack<String> operandStack) {
		return null;
	}

	@Override
	public int getDisplacement() {
		return 0;
	}

	@Override
	public String getOutputLine() {
		return "";
	}

}
