package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
/**
 * <b>Create new object</b>
 * The unsigned indexbyte1 and indexbyte2 are used to construct an index into the run-time constant pool of the current class (§2.6), 
 * where the value of the index is (indexbyte1 << 8) | indexbyte2. The run-time constant pool item at the index must be a symbolic reference 
 * to a class or interface type. The named class or interface type is resolved (§5.4.3.1) and should result in a class type. 
 * Memory for a new instance of that class is allocated from the garbage-collected heap, 
 * and the instance variables of the new object are initialized to their default initial values (§2.3, §2.4). 
 * The objectref, a reference to the instance, is pushed onto the operand stack.
 * @author jorge
 */
public class NewObj implements IInstructions {
	private int disp = 2;

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		Utilities utils = new Utilities();
		int cpIndex = utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		Object obj = constantPool.getConstant(cpIndex);
		if (obj.getClass().getName().indexOf("ConstantClass") == -1) return null;
		ConstantClass constantClass = (ConstantClass) obj;
		obj = constantPool.getConstant(constantClass.getNameIndex());
		if (obj.getClass().getName().indexOf("ConstantUtf8") == -1) return null;
		
		ConstantUtf8 utf8 = (ConstantUtf8)obj;
		operandStack.push(utils.trim(utf8.getBytes()));
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
}
