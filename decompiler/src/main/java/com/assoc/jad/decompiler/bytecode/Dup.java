package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public class Dup implements IInstructions {
	private int disp = 0;

	@Override
	public String execute(Method method, int i, JavaClass javaClass, Stack<String> operandStack) {
		operandStack.push(operandStack.peek());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}

}
