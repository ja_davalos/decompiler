package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

/**
 * Store int (0-3) into local variable 
 * The &#60;X> must be an index into the local variable array of the current frame (§2.6).
 * The value on the top of the operand stack must be of type int. 
 * It is popped from the operand stack, and the value of the local variable at &#60;X> is set to value.
 */
public class IntStore_X implements IInstructions {
	private int disp = 0;
	private int instValue;
	private StringBuilder result = new StringBuilder();

	public IntStore_X(int value) {
		this.instValue = value;
	}

	@Override
	public String execute(Method method,int ndx,JavaClass javaClass,Stack<String> operandStack) {
		result.setLength(0);
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[instValue];
		Utilities utils = new Utilities();
		result = utils.BldLocalVariableText(lv,operandStack);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
}
