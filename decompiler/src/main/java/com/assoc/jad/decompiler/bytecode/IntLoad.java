package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

public class IntLoad implements IInstructions {
	private int disp = 1;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		result = new StringBuilder();
		Utilities utils = new Utilities();
		int lvIndex =  utils.getIndexFromCode(method, ndx,disp);
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[lvIndex]; 
		operandStack.push(lv.getName());
		return null;
	}

	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		return result.toString();
	}

}
