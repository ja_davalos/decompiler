package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

/*
 * load a double from local variable pointed by value.0-3
 */
public class DLoad_X implements IInstructions {
	private int disp = 0;
	@SuppressWarnings("unused")
	private int value;
	
	public DLoad_X(int value) {
		this.value = value;
	}

	@Override
	public String execute(Method method,int ndx,JavaClass javaClass, Stack<String> operandStack) {
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}

}
