package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public class GoTo implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		result = new StringBuilder();
		int offset = method.getCode().getCode()[ndx+1] << 8 |  method.getCode().getCode()[ndx+2];
		int ptr = ndx + offset;
		result = disassmbleCode(method,javaClass,ptr) ;
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	private StringBuilder disassmbleCode(Method method,JavaClass javaClass,int ptr) {
		Stack<String> operandStack = new Stack<String>();
		StringBuilder javaCode = new StringBuilder();
		byte[] bytecodes = method.getCode().getCode();
		boolean finished = false;
		int i=ptr;
		for (;i<bytecodes.length && !finished;i++) {
			IInstructions instructionClass = InstructionClazzes.instructions.get(bytecodes[i]);
			if (instructionClass == null) {
				String wrkString = String.format("instruction not implemented hex=%2x dec=%2d", bytecodes[i],bytecodes[i]);
				System.err.println(wrkString);
				continue;
			}
			instructionClass.execute(method,i,javaClass,operandStack);
			
			i += instructionClass.getDisplacement();
			if (instructionClass.getOutputLine().length() > 0) {
				javaCode.append(instructionClass.getOutputLine());
				break;
			}
		}
		Utilities.loopEntryToExitPoint.put(ptr, ++i);
		return javaCode;
	}
}
