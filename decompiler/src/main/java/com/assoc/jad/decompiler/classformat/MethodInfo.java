package com.assoc.jad.decompiler.classformat;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class MethodInfo {
    private byte[] accessFlags = new byte[2];
    private byte[] nameIndex = new byte[2];
    private byte[] descriptorIndex = new byte[2];
    private byte[] attributesCount = new byte[2];
    private ArrayList<Object> attributeInfo = new ArrayList<Object>();
	private int UAttributesCount;
    
	public byte[] getAccessFlags() {
		return accessFlags;
	}
	public void setAccessFlags(byte[] accessFlags) {
		this.accessFlags = accessFlags;
	}
	public byte[] getNameIndex() {
		return nameIndex;
	}
	public void setNameIndex(byte[] nameIndex) {
		this.nameIndex = nameIndex;
	}
	public byte[] getDescriptorIndex() {
		return descriptorIndex;
	}
	public void setDescriptorIndex(byte[] descriptorIndex) {
		this.descriptorIndex = descriptorIndex;
	}
	public byte[] getAttributesCount() {
		return attributesCount;
	}
	public void setAttributesCount(byte[] attributesCount) {
		this.attributesCount = attributesCount;
		setUAttributesCount(ByteBuffer.wrap(attributesCount).getShort() & 0x0000ffff);
	}
	public int getUAttributesCount() {
		return UAttributesCount;
	}
	public void setUAttributesCount(int uAttributesCount) {
		UAttributesCount = uAttributesCount;
	}
	public ArrayList<Object> getAttributeInfo() {
		return attributeInfo;
	}
	public void addAttributeslInfo(Object attributeslInfo) {
		attributeInfo.add(attributeslInfo);
	}

}
