package com.assoc.jad.decompiler.bytecode;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Stack;

import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

public class Utilities {
	public static HashMap<String,String> methodInitVars = new HashMap<String,String>();
	public static HashMap<Integer,Integer> loopEntryToExitPoint = new HashMap<Integer,Integer>();

	public StringBuilder BldLocalVariableText(LocalVariable lv,Stack<String> operandStack) {
		StringBuilder result = new StringBuilder();
		result.setLength(0);
		result.append(getSignature(lv.getSignature())).append(" ");
		result.append(lv.getName()).append(" = ").append(operandStack.pop()).append(';').append(System.lineSeparator());
		methodInitVars.put(lv.getName(), result.toString());
		return result;
	}
	public int getIndexFromCode(Method method,int ndx,int codeGet) {
		byte[] wrk = new byte[2];
		byte[] codes = method.getCode().getCode();
		
		wrk[0] = 0;
		if (codeGet > 1) wrk[0] = codes[++ndx];
		wrk[1] = codes[++ndx];
		
		int cpIndex =  ByteBuffer.wrap(wrk).getShort() & 0x0000ffff;
		return cpIndex;
	}
	private String getSignature(String symbol) {
		String name = symbol.substring(0, 1);
		switch (name) {
			case "B":	name = "byte";break;
			case "C":	name = "char";break;
			case "D":	name = "double";break;
			case "F":	name = "float";break;
			case "I":	name = "int";break;
			case "J":	name = "long";break;
			case "S":	name = "short";break;
			case "Z":	name = "boolean";break;
			case "[":	name = "reference";break;
			case "L":
				int ndx = symbol.lastIndexOf('/');
				name = symbol.substring(++ndx,symbol.length()-1);
				break;
		}
		return name;
	}
	/*
	 * getters and setters
	 */
	public String trim(String name) {
		String tag = "java/lang/";
		int ndx = name.lastIndexOf(tag);
		if (ndx != -1) name = name.substring(ndx+tag.length());
		return name;
	}
}
