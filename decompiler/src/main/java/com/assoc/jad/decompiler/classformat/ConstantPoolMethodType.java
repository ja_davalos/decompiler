package com.assoc.jad.decompiler.classformat;

public class ConstantPoolMethodType {

	    private byte tag;
	    private byte[] descriptorIndex = new byte[2];

		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
		public byte[] getDescriptorIndex() {
			return descriptorIndex;
		}
		public void setDescriptorIndex(byte[] descriptorIndex) {
			this.descriptorIndex = descriptorIndex;
		}
}