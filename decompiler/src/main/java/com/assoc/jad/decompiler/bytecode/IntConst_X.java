package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

/**
 * Push the int constant &#60;X> (-1, 0, 1, 2, 3, 4 or 5) onto the operand stack.
 */
public class IntConst_X implements IInstructions {
	private int disp = 0;
	private int InstValue;
	
	public IntConst_X(int value) {
		this.InstValue = value;
	}

	@Override
	public String execute(Method method, int ndx,JavaClass javaClass, Stack<String> operandStack) {
		Integer VALUE = InstValue;
		operandStack.push(VALUE.toString());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		return "";
	}

}
