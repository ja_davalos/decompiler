package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

public class AStore implements IInstructions {
	private int disp = 1;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		result = new StringBuilder();
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[cpIndex]; 
		result = utils.BldLocalVariableText(lv,operandStack);
		return null;
	}

	@Override
	public int getDisplacement() {
		// TODO Auto-generated method stub
		return disp;
	}

	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return result.toString();
	}

}
