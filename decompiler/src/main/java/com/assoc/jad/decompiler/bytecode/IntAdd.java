package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public class IntAdd implements IInstructions {
	private int disp = 0;

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		String parm1 = operandStack.pop();
		String parm2 = operandStack.pop();
		operandStack.push(parm2+"+"+parm1);
		return null;
	}

	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		return "";
	}

}
