package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantMethodref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public class InvokeVirtual implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		result = new StringBuilder();
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		ConstantMethodref methodRef = (ConstantMethodref) constantPool.getConstant(cpIndex);
		cpIndex = methodRef.getClassIndex();
		ConstantClass constantClass = (ConstantClass)constantPool.getConstant(cpIndex);
		ConstantUtf8 classUtf8 = (ConstantUtf8)constantPool.getConstant(constantClass.getNameIndex());
		ConstantNameAndType nameAndType = (ConstantNameAndType) constantPool.getConstant(methodRef.getNameAndTypeIndex());
		
		if (classUtf8.getBytes().equals("java/lang/StringBuilder")) {
			String parm1 = operandStack.pop();
			String parm2 = operandStack.pop();
			if (nameAndType.getName(constantPool).indexOf("toString") != -1) {
				  Matcher m = Pattern.compile("\\((.*?)\\)").matcher(parm1);
				  StringBuilder rebldparm = new StringBuilder();
				  while(m.find()) {
					  rebldparm.append(m.group(1)).append('+');
				  }
				  rebldparm.setLength(rebldparm.length()-1);
				  operandStack.push(rebldparm.toString());
			} 
			else operandStack.push(parm2+"."+nameAndType.getName(constantPool)+"("+parm1+")");
			return null;
		}
		
		result.append(classUtf8.getBytes()).append(' ' );
		result.append(nameAndType.getName(constantPool)).append(' ');
		return null;
	}

	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		return result.toString();
	}

}
