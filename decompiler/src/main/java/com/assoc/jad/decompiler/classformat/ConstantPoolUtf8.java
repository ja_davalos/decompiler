package com.assoc.jad.decompiler.classformat;

import java.nio.ByteBuffer;

public class ConstantPoolUtf8 {

	    private byte tag;
	    private byte[] length = new byte[2];
	    private byte[] bytes = null;
	    private int uLength;
	    
		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
		private void bldBytes() {
			uLength = ByteBuffer.wrap(length).getShort() & 0x0000ffff;
			bytes = new byte[uLength];
		}
		public byte[] getBytes() {
			return bytes;
		}
		public void setBytes(byte[] bytes) {
			this.bytes = bytes;
		}
		public byte[] getLength() {
			return length;
		}
		public void setLength(byte[] length) {
			this.length = length;
			this.bldBytes();
		}
		public int getuLength() {
			return uLength;
		}
}