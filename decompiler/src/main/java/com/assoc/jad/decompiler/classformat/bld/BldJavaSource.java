package com.assoc.jad.decompiler.classformat.bld;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Stack;

import org.apache.bcel.Const;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.decompiler.bytecode.IInstructions;
import com.assoc.jad.decompiler.bytecode.InstructionClazzes;
import com.assoc.jad.decompiler.bytecode.Utilities;

public class BldJavaSource {

	private FileInputStream fileInputStream;
	private String filename;
	private StringBuilder indentation = new StringBuilder("\t");

	public BldJavaSource(FileInputStream inpf, String filename) {
		this.fileInputStream = inpf;
		this.filename = filename;
		bldJavaSource();
	}

	private void bldJavaSource() {

		try {
	        ClassParser classParser = new ClassParser(fileInputStream, filename);
	        JavaClass javaClass = classParser.parse();
	        setClassHeader(javaClass);
	        for (Method method : javaClass.getMethods()) {
	        	System.out.println(indentation+method.toString()+" {");
	        	disassmbleCode(method,javaClass);
	        	System.out.println(indentation+"}");
	        }
		} catch (Exception e) {e.printStackTrace();}
		System.out.println("}");
	}

	private void disassmbleCode(Method method,JavaClass javaClass) {
		Utilities.loopEntryToExitPoint = new HashMap<Integer,Integer>();
		Utilities.methodInitVars = new HashMap<String,String>();
		Stack<String> operandStack = new Stack<String>();
		StringBuilder javaCode = new StringBuilder();
		int len = this.indentation.length();
		indentation.append("\t");
		byte[] bytecodes = method.getCode().getCode();
		
		for (int i=0;i<bytecodes.length;i++) {
			if (Utilities.loopEntryToExitPoint.get(i) != null) {
				javaCode.append(indentation).append("}").append(System.lineSeparator());
				i = Utilities.loopEntryToExitPoint.get(i);
			}
			IInstructions instructionClass = InstructionClazzes.instructions.get(bytecodes[i]);
			if (instructionClass == null) {
				String wrkString = String.format("instruction not implemented hex=%2x dec=%2d", bytecodes[i],bytecodes[i]);
				System.err.println(wrkString);
				continue;
			}
			instructionClass.execute(method,i,javaClass,operandStack);
			
			if (instructionClass.getOutputLine().length() > 0)
				javaCode.append(indentation+instructionClass.getOutputLine());
			i += instructionClass.getDisplacement();
		}
		System.out.println(javaCode.toString());
		indentation.setLength(len);
	}
	private void setClassHeader(JavaClass javaClass) {
		
		StringBuilder sb = resolveClassAccess(javaClass.getAccessFlags());
		int ndx = javaClass.getClassName().lastIndexOf('.');
		sb.append(javaClass.getClassName().substring(++ndx)).append(" ");
		sb.append(" extends ").append(javaClass.getSuperclassName());
		System.out.println(sb.toString()+ "{");
	}

	private StringBuilder resolveClassAccess(int access) {
		StringBuilder sb = new StringBuilder();
		
		if ((access & Const.ACC_PUBLIC) == Const.ACC_PUBLIC) sb.append("public ");
		if ((access & Const.ACC_FINAL) == Const.ACC_FINAL) sb.append("final ");
		if ((access & Const.ACC_INTERFACE) == Const.ACC_INTERFACE) sb.append("interface ");
		if (!((access & Const.ACC_INTERFACE) == Const.ACC_INTERFACE)) sb.append("class ");
		if ((access & Const.ACC_ABSTRACT) == Const.ACC_ENUM) sb.append("abstract ");
		if ((access & Const.ACC_SYNTHETIC) == Const.ACC_ENUM) sb.append("synthetic ");
		if ((access & Const.ACC_ANNOTATION) == Const.ACC_ENUM) sb.append("annotation ");
		if ((access & Const.ACC_ENUM) == Const.ACC_ENUM) sb.append("enum ");
		return sb;
	}
}