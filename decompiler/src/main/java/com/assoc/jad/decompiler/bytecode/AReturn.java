package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public class AReturn implements IInstructions {
	private int disp = 0;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int i, JavaClass javaClass, Stack<String> operandStack) {
		result.setLength(0);
		result.append("return ").append(operandStack.pop()).append(';');
		return null;
	}

	@Override
	public int getDisplacement() {
		// TODO Auto-generated method stub
		return disp;
	}

	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return result.toString();
	}

}
