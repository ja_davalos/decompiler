package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantMethodref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
/**
 * The unsigned indexbyte1 and indexbyte2 are used to construct an index into the run-time constant pool of the current class (§2.6),
 *  where the value of the index is (indexbyte1 << 8) | indexbyte2. 
 *  <b>The run-time constant pool item at that index must be a symbolic reference to a method (§5.1),</b> 
 *  which gives the name and descriptor (§4.3.3) of the method as well as a symbolic reference to the class in which the method is to be found. 
 *  The named method is resolved (§5.4.3.3). 
 *  The resolved method must not be an instance initialization method (§2.9) or the class or interface initialization method (§2.9). 
 *  It must be static, and therefore cannot be abstract.
 * @author jorge
 *
 */
public class InvokeStatic implements IInstructions {
	StringBuilder result = new StringBuilder();
	private int disp = 2;

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		result = new StringBuilder();
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		
		ConstantMethodref methodRef = (ConstantMethodref) constantPool.getConstant(cpIndex);
		ConstantClass constantClass = (ConstantClass)constantPool.getConstant(methodRef.getClassIndex()); 
		ConstantUtf8 classConstantUtf8 = (ConstantUtf8)constantPool.getConstant(constantClass.getNameIndex());
		ConstantNameAndType nameAndType = (ConstantNameAndType) constantPool.getConstant(methodRef.getNameAndTypeIndex());
		
		if (classConstantUtf8.getBytes().equals("java/lang/Object"))
			result.append(nameAndType.getName(constantPool)).append(';').append(System.lineSeparator());
		else {
			String classname = classConstantUtf8.getBytes();
			if (classname.equals("java/lang/String")) return null;
			int ndx2 = classname.lastIndexOf('/');
			String newoper = "new "+classname.substring(++ndx2)+"("+operandStack.pop()+")";
			operandStack.push(newoper);
		}
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp ;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}

}
