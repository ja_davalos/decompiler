package com.assoc.jad.decompiler.attributes;

public class ConstantValue {
    private byte[] attributeNameIndex = new byte[2];
    private byte[] attributeLength = new byte[4];
    private byte[] constantvalueIndex = new byte[2];
    
	public byte[] getAttributeNameIndex() {
		return attributeNameIndex;
	}
	public void setAttributeNameIndex(byte[] attributeNameIndex) {
		this.attributeNameIndex = attributeNameIndex;
	}
	public byte[] getAttributeLength() {
		return attributeLength;
	}
	public void setAttributeLength(byte[] attributeLength) {
		this.attributeLength = attributeLength;
	}
	public byte[] getConstantvalueIndex() {
		return constantvalueIndex;
	}
	public void setConstantvalueIndex(byte[] constantvalueIndex) {
		this.constantvalueIndex = constantvalueIndex;
	}
}
