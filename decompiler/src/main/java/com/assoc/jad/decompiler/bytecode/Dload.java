package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

/*
 *load a double value from a local variable #index
 */
public class Dload implements IInstructions {
	private int disp = 1;

	@Override
	public String execute(Method method,int ndx,JavaClass javaClass, Stack<String> operandStack) {
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}

}
