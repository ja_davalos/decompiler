package com.assoc.jad.decompiler.classformat;

public class ConstantPoolString {

	    private byte tag;
	    private byte[] stringIndex = new byte[2];
	    
		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
		public byte[] getStringIndex() {
			return stringIndex;
		}
		public void setStringIndex(byte[] stringIndex) {
			this.stringIndex = stringIndex;
		}
}

