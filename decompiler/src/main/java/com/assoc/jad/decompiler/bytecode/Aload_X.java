package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.*;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

/**
 * load a <b>reference</b> onto the stack from local variable <br/>
 * The &#60;X> must be an index into the local variable array of the current frame (§2.6). 
 * The local variable at &#60;X> must contain a reference. The objectref in the local variable at &#60;X> is pushed onto the operand stack.

Notes
 */
public class Aload_X implements IInstructions {
	private int disp = 0;
	private int instValue;
	
	public Aload_X(int value) {
		this.instValue = value;
	}

	@Override
	public String execute(Method method,int ndx,JavaClass javaClass, Stack<String> operandStack) {
		ConstantPool constantPool = method.getConstantPool();
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[instValue]; 
		Object obj = constantPool.getConstant(lv.getNameIndex());
		if (obj.getClass().getName().indexOf("ConstantUtf8") == -1) return null;
		
		String poolValue =  ((ConstantUtf8)obj).getBytes();
		operandStack.push(poolValue);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
}
