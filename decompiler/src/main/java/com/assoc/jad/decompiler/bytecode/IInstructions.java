package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public interface IInstructions {
	String execute(Method method,int ndx, JavaClass javaClass, Stack<String> operandStack);
	int getDisplacement();
	String getOutputLine();
}
