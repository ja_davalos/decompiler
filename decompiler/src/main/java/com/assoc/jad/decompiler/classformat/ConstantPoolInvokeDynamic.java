package com.assoc.jad.decompiler.classformat;

public class ConstantPoolInvokeDynamic {

	    private byte tag;
	    private byte[] bootstrapMethodAttrIndex = new byte[2];
	    private byte[] nameAndTypeIndex = new byte[2];

		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
		public byte[] getBootstrapMethodAttrIndex() {
			return bootstrapMethodAttrIndex;
		}
		public void setBootstrapMethodAttrIndex(byte[] bootstrapMethodAttrIndex) {
			this.bootstrapMethodAttrIndex = bootstrapMethodAttrIndex;
		}
		public byte[] getNameAndTypeIndex() {
			return nameAndTypeIndex;
		}
		public void setNameAndTypeIndex(byte[] nameAndTypeIndex) {
			this.nameAndTypeIndex = nameAndTypeIndex;
		}
}