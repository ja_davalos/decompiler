package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

public class If_IntCmp implements IInstructions {

	private String cmp;
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	public If_IntCmp(String cmp) {
		this.cmp = cmp;
	}

	@Override
	public String execute(Method method, int ndx, JavaClass javaClass, Stack<String> operandStack) {
		result = new StringBuilder();
		String symbol = cmp;
		switch (cmp) {
		case "eq":	symbol = " = ";break;
		case "ne":	symbol = " != ";break;
		case "lt":	symbol = " < ";break;
		case "ge":	symbol = " >= ";break;
		case "gt":	symbol = " > ";break;
		case "le":	symbol = " <= ";break;
		}
		String value = operandStack.pop();
		String var = operandStack.pop();
		//result.append("for ("+Utilities.methodInitVars.get(var)+var+symbol+value);
		result.append("while ("+var+symbol+value+") {"+System.lineSeparator());
		return null;
	}

	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		return result.toString();
	}
}
