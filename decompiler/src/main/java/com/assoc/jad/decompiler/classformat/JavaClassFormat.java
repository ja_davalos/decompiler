package com.assoc.jad.decompiler.classformat;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import com.assoc.jad.decompiler.attributes.ConstantValue;

public class JavaClassFormat {
	
    private byte[] magic = new byte[4];
    private byte[] minorVersion = new byte[2];
    private byte[] majorVersion = new byte[2];
    private byte[] constantPoolCount = new byte[2];
    private ArrayList<Object> constantPoolInfo = new ArrayList<Object>();
    private byte[] accessFlags = new byte[2];
    private byte[] thisClass = new byte[2];
    private byte[] superClass = new byte[2];
    private byte[] interfacesCount = new byte[2];
    private ArrayList<byte[]> interfaces = new ArrayList<byte[]>();
    private byte[] fieldsCount = new byte[2];
    private ArrayList<Object> fieldInfo = new ArrayList<Object>();
    private byte[] methodsCount = new byte[2];
    private ArrayList<Object> methods = new ArrayList<Object>();
    private byte[] attributesCount = new byte[2];
    private ArrayList<Object> attributes = new ArrayList<Object>();
    
    private int uInterfacesCount;
    private int uFieldsCount;
	private int uMethodsCount;
    private HashMap<String,Object> atributeClasses = new HashMap<String,Object>();
	{
    	atributeClasses.put("ConstantValue",new ConstantValue());
    }
/*
 * getters and setters
 */
	public byte[] getMagic() {
		return magic;
	}
	public void setMagic(byte[] magic) {
		this.magic = magic;
	}
	public byte[] getMinorVersion() {
		return minorVersion;
	}
	public void setMinorVersion(byte[] minorVersion) {
		this.minorVersion = minorVersion;
	}
	public byte[] getMajorVersion() {
		return majorVersion;
	}
	public void setMajorVersion(byte[] majorVersion) {
		this.majorVersion = majorVersion;
	}
	public byte[] getConstantPoolCount() {
		return constantPoolCount;
	}
	public void setConstantPoolCount(byte[] constantPoolCount) {
		this.constantPoolCount = constantPoolCount;
	}
	public ArrayList<Object> getConstantPoolInfo() {
		return constantPoolInfo;
	}
	public void addConstantPoolObj(Object constantPool) {
		this.constantPoolInfo.add(constantPool);
	}
	public byte[] getAccessFlags() {
		return accessFlags;
	}
	public void setAccessFlags(byte[] accessFlags) {
		this.accessFlags = accessFlags;
	}
	public byte[] getThisClass() {
		return thisClass;
	}
	public void setThisClass(byte[] thisClass) {
		this.thisClass = thisClass;
	}
	public byte[] getSuperClass() {
		return superClass;
	}
	public void setSuperClass(byte[] superClass) {
		this.superClass = superClass;
	}
	public byte[] getInterfacesCount() {
		return interfacesCount;
	}
	public void setInterfacesCount(byte[] interfacesCount) {
		this.interfacesCount = interfacesCount;
		setuInterfacesCount(ByteBuffer.wrap(interfacesCount).getShort() & 0x0000ffff);
	}
	public ArrayList<byte[]> getInterfaces() {
		return interfaces;
	}
	public void setInterfaces(ArrayList<byte[]> interfaces) {
		this.interfaces = interfaces;
	}
	public int getuInterfacesCount() {
		return uInterfacesCount;
	}
	public void setuInterfacesCount(int uInterfacesCount) {
		this.uInterfacesCount = uInterfacesCount;
	}
	public void addinterface(byte[] anInterface) {
		interfaces.add(anInterface);
	}
	public byte[] getFieldsCount() {
		return fieldsCount;
	}
	public void setFieldsCount(byte[] fieldsCount) {
		this.fieldsCount = fieldsCount;
		setuFieldsCount(ByteBuffer.wrap(fieldsCount).getShort() & 0x0000ffff);
	}
	public ArrayList<Object> getFieldInfo() {
		return fieldInfo;
	}
	public void addFieldInfo(Object fieldInfo) {
		this.fieldInfo.add(fieldInfo);
	}
	public int getuFieldsCount() {
		return uFieldsCount;
	}
	public void setuFieldsCount(int uFieldsCount) {
		this.uFieldsCount = uFieldsCount;
	}
	public byte[] getMethodsCount() {
		return methodsCount;
	}
	public void setMethodsCount(byte[] methodsCount) {
		this.methodsCount = methodsCount;
		setuMethodsCount(ByteBuffer.wrap(methodsCount).getShort() & 0x0000ffff);
	}
	public ArrayList<Object> getMethods() {
		return methods;
	}
	public void addMethods(Object method) {
		this.methods.add(method);
	}
	public byte[] getAttributesCount() {
		return attributesCount;
	}
	public void setAttributesCount(byte[] attributesCount) {
		this.attributesCount = attributesCount;
	}
	public ArrayList<Object> getAttributes() {
		return attributes;
	}
	public void setAttributes(ArrayList<Object> attributes) {
		this.attributes = attributes;
	}
	public int getuMethodsCount() {
		return uMethodsCount;
	}
	public void setuMethodsCount(int uMethodsCount) {
		this.uMethodsCount = uMethodsCount;
	}
    public Object getAtributeClass(String key) {
		return atributeClasses.get(key);
	}
}
