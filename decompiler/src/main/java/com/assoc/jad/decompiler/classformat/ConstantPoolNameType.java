package com.assoc.jad.decompiler.classformat;

public class ConstantPoolNameType {

	    private byte tag;
	    private byte[] nameIndex = new byte[2];
	    private byte[] descriptorIndex = new byte[2];
	    
		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
		public byte[] getDescriptorIndex() {
			return descriptorIndex;
		}
		public void setDescriptorIndex(byte[] descriptorIndex) {
			this.descriptorIndex = descriptorIndex;
		}
		public byte[] getNameIndex() {
			return nameIndex;
		}
		public void setNameIndex(byte[] nameIndex) {
			this.nameIndex = nameIndex;
		}
}