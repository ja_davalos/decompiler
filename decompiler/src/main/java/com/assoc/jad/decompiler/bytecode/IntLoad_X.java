package com.assoc.jad.decompiler.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

/*
 * The <n> must be an index into the local variable array of the current frame (§2.6).
 *  The local variable at <n> must contain an int. The value of the local variable at <n> is pushed onto the operand stack.
 */
public class IntLoad_X implements IInstructions {
	private int disp = 0;
	private int instValue;
	
	public IntLoad_X(int value) {
		this.instValue = value;
	}

	@Override
	public String execute(Method method,int ndx,JavaClass javaClass, Stack<String> operandStack) {
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[instValue]; 
		operandStack.push(lv.getName());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		return "";
	}

}
