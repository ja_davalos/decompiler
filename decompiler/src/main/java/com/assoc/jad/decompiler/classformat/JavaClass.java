package com.assoc.jad.decompiler.classformat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.bcel.Constants;

import com.assoc.jad.decompiler.classformat.bld.BldJavaSource;

public class JavaClass {
	private FileInputStream inpf = null;
	private byte[] bytes = null;
	private int ctr = 0;
	private String filename = null;
	private JavaClassFormat javaClassFormat = new JavaClassFormat();
	
	public JavaClass(File file) throws IOException {
		inpf = new FileInputStream(file);
		new BldJavaSource(inpf,file.getAbsolutePath());
	}
	private void bldJavaFormat() throws IOException {
		int len = 0;
		while ((len = inpf.read(bytes,len,bytes.length-len)) != -1);	//read the full file into array.
		
		if (!isMagicNumber()) throw new IOException("File "+filename+" is not a java class");
		
		javaClassFormat.setMinorVersion(bldArray(2));
		javaClassFormat.setMajorVersion(bldArray(2));
		int cpCount = constantPoolCount();
		for (int i=0;i<cpCount-1;i++ ) {
			constantPoolInfo();
		}
		javaClassFormat.setAccessFlags(bldArray(2));
		javaClassFormat.setThisClass(bldArray(2));
		javaClassFormat.setSuperClass(bldArray(2));
		javaClassFormat.setInterfacesCount(bldArray(2));
		for (int i=0;i<javaClassFormat.getuInterfacesCount();i++ ) {
			javaClassFormat.addinterface(bldArray(2));
		}
		javaClassFormat.setFieldsCount(bldArray(2));
		for (int i=0;i<javaClassFormat.getuFieldsCount();i++ ) {
			addFieldsInfo();
		}
		javaClassFormat.setMethodsCount(bldArray(2));
		for (int i=0;i<javaClassFormat.getuMethodsCount();i++ ) {
			addMethodsInfo();
		}
		System.out.println("ctr="+ctr+" class length="+bytes.length);
	}
	private void addMethodsInfo() {
		MethodInfo methodInfo = new MethodInfo();
		methodInfo.setAccessFlags(bldArray(2));
		methodInfo.setNameIndex(bldArray(2));
		methodInfo.setDescriptorIndex(bldArray(2));
		methodInfo.setAttributesCount(bldArray(2));
		for (int i=0;i<methodInfo.getUAttributesCount();i++) {
			methodInfo.addAttributeslInfo(bldAttributeInfo());
		}
		javaClassFormat.addMethods(methodInfo);		
	}
	private void addFieldsInfo() {
		FieldInfo fieldInfo = new FieldInfo();
		fieldInfo.setAccessFlags(bldArray(2));
		fieldInfo.setNameIndex(bldArray(2));
		fieldInfo.setDescriptorIndex(bldArray(2));
		fieldInfo.setAttributesCount(bldArray(2));
		for (int i=0;i<fieldInfo.getuAttributesCount();i++) {
			fieldInfo.addAttributeslInfo(bldAttributeInfo());
		}
		javaClassFormat.addFieldInfo(fieldInfo);
	}
	private AttributeInfo bldAttributeInfo() {
		AttributeInfo attributeInfo = new AttributeInfo();
		attributeInfo.setAttributeNameIndex(bldArray(2));
		attributeInfo.setAttributeLength(bldArray(4));
		for (int j=0;j<attributeInfo.getUAttributeLength();j++) {
			attributeInfo.addInfo(bytes[ctr++]);
		}
		return attributeInfo;
	}
	private void constantPoolInfo() {

		byte tag = bytes[ctr];
		switch (tag) {
			case Constants.CONSTANT_Class: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolClass()); 
				break;
			}
			case Constants.CONSTANT_Fieldref: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolFieldRef()); 
				break;
			}
			case Constants.CONSTANT_Methodref: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolMethodRef()); 
				break;
			}
			case Constants.CONSTANT_InterfaceMethodref: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolInterfaceMethodRef()); 
				break;
			}
			case Constants.CONSTANT_String: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolString()); 
				break;
			}
			case Constants.CONSTANT_Integer: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolInteger()); 
				break;
			}
			case Constants.CONSTANT_Float: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolFloat()); 
				break;
			}
			case Constants.CONSTANT_Long: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolLong()); 
				break;
			}
			case Constants.CONSTANT_NameAndType: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolNameType()); 
				break;
			}
			case Constants.CONSTANT_Utf8: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolUtf8()); 
				break;
			}
			case Constants.CONSTANT_Double: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolDouble()); 
				break;
			}
			case 15: {	//TODO
				javaClassFormat.addConstantPoolObj(bldConstantPoolMethodHandle()); 
				break;
			}
			case 16: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolMethodType()); 
				break;
			}
			case 18: {
				javaClassFormat.addConstantPoolObj(bldConstantPoolInvokeDynamic()); 
				break;
			}
			default:
			  System.out.println("Constant Pool invalid or not implement! tag="+tag);
		}
	}
	private ConstantPoolInvokeDynamic bldConstantPoolInvokeDynamic() {
		ConstantPoolInvokeDynamic constantPoolInvokeDynamic = new ConstantPoolInvokeDynamic();
		constantPoolInvokeDynamic.setTag(bytes[ctr++]);
		constantPoolInvokeDynamic.setBootstrapMethodAttrIndex(bldArray(2));
		constantPoolInvokeDynamic.setNameAndTypeIndex(bldArray(2));
		return constantPoolInvokeDynamic;
	}
	private ConstantPoolMethodType bldConstantPoolMethodType() {
		ConstantPoolMethodType constantPoolMethodType = new ConstantPoolMethodType();
		constantPoolMethodType.setTag(bytes[ctr++]);
		constantPoolMethodType.setDescriptorIndex(bldArray(2));
		return constantPoolMethodType;
	}
	private ConstantPoolMethodHandle bldConstantPoolMethodHandle() {
		ConstantPoolMethodHandle constantPoolMethodHandle = new ConstantPoolMethodHandle();
		constantPoolMethodHandle.setTag(bytes[ctr++]);
		constantPoolMethodHandle.setReferenceKind(bytes[ctr++]);
		constantPoolMethodHandle.setReferenceIndex(bldArray(2));
		return constantPoolMethodHandle;
	}
	private ConstantPoolUtf8 bldConstantPoolUtf8() {
		ConstantPoolUtf8 constantPoolUtf8 = new ConstantPoolUtf8();
		constantPoolUtf8.setTag(bytes[ctr++]);
		constantPoolUtf8.setLength(bldArray(2));
		constantPoolUtf8.setBytes(bldArray(constantPoolUtf8.getuLength()));
		return constantPoolUtf8;
	}
	private ConstantPoolNameType bldConstantPoolNameType() {
		ConstantPoolNameType constantPoolNameType = new ConstantPoolNameType();
		constantPoolNameType.setTag(bytes[ctr++]);
		constantPoolNameType.setNameIndex(bldArray(2));
		constantPoolNameType.setDescriptorIndex(bldArray(2));
		return constantPoolNameType;
	}
	private ConstantPoolDouble bldConstantPoolDouble() {
		ConstantPoolDouble constantPoolDouble = new ConstantPoolDouble();
		constantPoolDouble.setTag(bytes[ctr++]);
		constantPoolDouble.setHighBytes(bldArray(4));
		constantPoolDouble.setLowBytes(bldArray(4));
		return constantPoolDouble;
	}
	private ConstantPoolLong bldConstantPoolLong() {
		ConstantPoolLong constantPoolLong = new ConstantPoolLong();
		constantPoolLong.setTag(bytes[ctr++]);
		constantPoolLong.setHighBytes(bldArray(4));
		constantPoolLong.setLowBytes(bldArray(4));
		return constantPoolLong;
	}
	private ConstantPoolFloat bldConstantPoolFloat() {
		ConstantPoolFloat constantPoolFloat = new ConstantPoolFloat();
		constantPoolFloat.setTag(bytes[ctr++]);
		constantPoolFloat.setBytes(bldArray(4));
		return constantPoolFloat;
	}
	private ConstantPoolInteger bldConstantPoolInteger() {
		ConstantPoolInteger constantPoolInteger = new ConstantPoolInteger();
		constantPoolInteger.setTag(bytes[ctr++]);
		constantPoolInteger.setBytes(bldArray(4));
		return constantPoolInteger;
	}
	private ConstantPoolString bldConstantPoolString() {
		ConstantPoolString constantPoolString = new ConstantPoolString();
		constantPoolString.setTag(bytes[ctr++]);
		constantPoolString.setStringIndex(bldArray(2));
		return constantPoolString;
	}
	private ConstantPoolInterfaceMethodRef bldConstantPoolInterfaceMethodRef() {
		ConstantPoolInterfaceMethodRef constantPoolInterfaceMethodRef = new ConstantPoolInterfaceMethodRef();
		constantPoolInterfaceMethodRef.setTag(bytes[ctr++]);
		constantPoolInterfaceMethodRef.setClassIndex(bldArray(2));
		constantPoolInterfaceMethodRef.setNameTypeIndex(bldArray(2));
		return constantPoolInterfaceMethodRef;
	}
	private ConstantPoolMethodRef bldConstantPoolMethodRef() {
		ConstantPoolMethodRef constantPoolMethodRef = new ConstantPoolMethodRef();
		constantPoolMethodRef.setTag(bytes[ctr++]);
		constantPoolMethodRef.setClassIndex(bldArray(2));
		constantPoolMethodRef.setNameTypeIndex(bldArray(2));
		return constantPoolMethodRef;
	}
	private ConstantPoolFieldRef bldConstantPoolFieldRef() {
		ConstantPoolFieldRef constantPoolFieldRef = new ConstantPoolFieldRef();
		constantPoolFieldRef.setTag(bytes[ctr++]);
		constantPoolFieldRef.setClassIndex(bldArray(2));
		constantPoolFieldRef.setNameTypeIndex(bldArray(2));
		return constantPoolFieldRef;
	}
	private ConstantPoolClass bldConstantPoolClass() {
		ConstantPoolClass constantPoolClass = new ConstantPoolClass();
		constantPoolClass.setTag(bytes[ctr++]);
		constantPoolClass.setNameIndex(bldArray(2));
		return constantPoolClass;
	}
	private int constantPoolCount() {
		javaClassFormat.setConstantPoolCount(bldArray(2));
		return ByteBuffer.wrap(javaClassFormat.getConstantPoolCount()).getShort() & 0x0000ffff;
		
	}
	private byte[] bldArray(int len) {
		byte[] array = new byte[len];
		System.arraycopy(bytes, ctr, array, 0, array.length);
		ctr += array.length;
		return array;
	}
	private boolean isMagicNumber() {
		byte[] magic = {(byte)0xca,(byte)0xfe,(byte)0xba,(byte)0xbe};
		
		javaClassFormat.setMagic(magic);
		for (int i=0;i<magic.length;i++,ctr++) if (bytes[ctr] != magic[i]) return false;
		return true;
	}
}
