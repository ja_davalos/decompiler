package com.assoc.jad.decompiler.classformat;

public class ConstantPoolClass {

	    private byte tag;
	    private byte[] nameIndex = new byte[2];
	    
		public byte[] getNameIndex() {
			return nameIndex;
		}
		public void setNameIndex(byte[] nameIndex) {
			this.nameIndex = nameIndex;
		}
		public byte getTag() {
			return tag;
		}
		public void setTag(byte tag) {
			this.tag = tag;
		}
}

